version_info = (0, 5, 4)

__version__ = VERSION = '.'.join(map(str, version_info))
__project__ = PROJECT = 'django-markdown'
__author__ = AUTHOR = "Kirill Klenov <horneds@gmail.com>"
__license__ = LICENSE = "GNU LGPL"
